# Prerequisite

To start with CI / CD pipeline, you'll need to download and install the following tools :
- Vsode : https://code.visualstudio.com/download

- Docker : https://docs.docker.com/docker-for-windows/install/ (Docker Desktop for Windows)

!!! Attention
    Docker is using server side deamon to run containors. You will need to install or update WSL to WSL version 2.


- Git : https://git-scm.com/downloads

In order to begin with task automation, you'll need :
- Create a free account on GitLab.com

- An open source project available on GitLab https://gitlab.com/spring-petclinic-microservices/petclinic-microservices


## Create a new blanck project

There are two ways to start a new project :
1. On your GitLab account with **New project**
2. With git command 
    `git init`

## Git initialisation

User preference configuration :

`git config --global user.name "sername"`

`git config --global user.email "exemple@google.com"`

> --global parameter helps you maintain the same configuration for all repositories created.

## Clone repository

Cloning helps you to export you repository to local and open it with an IDE :

`git clone htpps://gitlab.com/[username]/[your_repository]`








